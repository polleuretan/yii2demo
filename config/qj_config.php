<?php

return [
    'rmq' => [
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'guest',
        'pass' => 'guest',
        'exchange' => 'queue_jobs',
    ]
];
