<?php

namespace app\components;

class BackgroundProcess
{
  private $command;
  private $pid;

  public function __construct($command)
  {
    $this->command = $command;
  }

  public static function findProcess($search)
  {
    $cmd = "ps aux | grep '{$search}'";
    $res = shell_exec($cmd);
    $res = preg_split("/\n/", $res);
    $res = array_reverse($res);
    $res = array_slice($res, 3);
    $pids = [];
    foreach ($res as $row) {
      $row = ereg_replace(" +"," ",$row);
      $cols = explode(' ', $row);
      $pids[] = $cols[1];
    }
    return $pids;
  }

  public static function killProcess($pid)
  {
    $cmd = "kill {$pid}";
    $res = shell_exec($cmd);
    return $res;
  }

  public function run($outputFile = '/dev/null')
  {
    return shell_exec($this->command . ' >' . $outputFile . ' 2>&1 & echo $!');
  }
}