<?php

namespace app\components;

use Yii;
use yii\base\BootstrapInterface;
use QueueJobs\Config;
class GlobalBootstrap implements BootstrapInterface
{
  public function bootstrap($app)
  {
    Config::setConfigFile(__DIR__.'/../config/qj_config.php');
  }
}