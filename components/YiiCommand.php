<?php

namespace app\components;

use Yii;
class YiiCommand
{
  private $yiic;
  public $action;
  public $command;
  public $arguments = array();


  public function __construct($command, $action = null)
  {
    $this->command = $command;
    $this->action = $action;
    $this->yiic = Yii::$app->getBasePath() . '/yii';
  }
  public function construct()
  {
    $route = $this->command;
    if ($this->action) {
      $route .= "/{$this->action}";
    }
    $res = array(
      $this->yiic,
      $route,
    );
    foreach ($this->arguments as $value) {
      $res[] = $value;
    }
    return join($res, ' ');
  }
}