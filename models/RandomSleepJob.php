<?php
namespace app\models;
use QueueJobs\Job;
class RandomSleepJob extends Job
{
  public function __construct()
  {
    parent::__construct('randomSleep');
  }
  public function run()
  {
    echo 'Sleeping till done...' . PHP_EOL;
    $progress = 0;
    while (true) {
      $done = rand(1, 10);
      $progress += $done;
      if ($progress >= 100) {
        break;
      }
      $this->progress($done);
      sleep(1);
    }
    echo 'Finished' . PHP_EOL;
  }
}