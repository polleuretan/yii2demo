<?php
namespace app\models;
use QueueJobs\Job;
class SleepJob extends Job
{
  public function __construct($time)
  {
    parent::__construct('sleep');
    $this->data['time'] = $time;
  }
  public function run()
  {
    echo 'Sleeping ' . $this->data['time'] . ' seconds...' . PHP_EOL;
    $step = floor(100 / $this->data['time']);
    for ($i=0; $i < $this->data['time']; $i++) { 
      $this->progress($step);
      sleep(1);
    }
    echo 'Finished' . PHP_EOL;
  }
}