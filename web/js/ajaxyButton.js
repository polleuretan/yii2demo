$(function(){
  $('body').on('click', '.ajaxy-button', function(e){
    var el = $(e.target),
      url = el.attr('href');
    if (!url) {
      url = el.closest('a, button').attr('href');
    }
    $.ajax({
      url: url
    });
    e.preventDefault();
  });
});