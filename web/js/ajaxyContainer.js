(function(w){
	var AjaxyContainer = function(options){
		var defaults = {
			refresh: 5
		};
		this.options = $.extend(defaults, options);
		this.run();
	};

	AjaxyContainer.prototype.run = function(){
		var self = this;
		w.setInterval(function(){
			$.ajax({
				url: self.options.url
			}).done(function(data){
				self.options.container.empty();
				self.options.container.append(data);
			});
		}, this.options.refresh * 1000);
	};

	w.AjaxyContainer = AjaxyContainer;
})(window);