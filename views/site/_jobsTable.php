<?php
use yii\helpers\ArrayHelper;
use yii\bootstrap\Progress;
use app\helpers\DateHelper;
use app\helpers\JobsHelper;
$this->title = 'Jobs';
$this->registerJsFile(\Yii::$app->urlManager->getHostInfo(). '/js/jobs.js', [\yii\web\JqueryAsset::className()]);
?>
<table class="table table-hover table-bordered">
    <tr>
        <th>Type</th>
        <th>Status</th>
        <th>Progress</th>
        <th>Queued</th>
        <th>Started</th>
        <th>Finished</th>
        <th>Remove</th>
    </tr>
    <?php foreach ($jobs as $job): ?>
    <tr>
        <td><?= ArrayHelper::getValue($job, 'type', ''); ?></td>
        <td><?= JobsHelper::textStatus(ArrayHelper::getValue($job, 'status', '')); ?></td>
        <td style="width: 20%"><?= Progress::widget([
            'percent' => ArrayHelper::getValue($job, 'progress', ''),
        ]); ?></td>
        <td><?= DateHelper::mysqlFormat(ArrayHelper::getValue($job, 'queued', '')); ?></td>
        <td><?= DateHelper::mysqlFormat(ArrayHelper::getValue($job, 'started', '')); ?></td>
        <td><?= DateHelper::mysqlFormat(ArrayHelper::getValue($job, 'finished', '')); ?></td>
        <td>
            <div class="btn-group">
                <a href="<?= \Yii::$app->urlManager->createUrl(['site/deletejob', 'id' => $job['id']]); ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

