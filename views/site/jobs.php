<?php
$this->title = 'Jobs';
$host = \Yii::$app->urlManager->getHostInfo();
$this->registerJsFile($host . '/js/jobs.js', [\yii\web\JqueryAsset::className()]);
$this->registerJsFile($host . '/js/ajaxyContainer.js', [\yii\web\JqueryAsset::className()]);
$this->registerJsFile($host . '/js/ajaxyButton.js', [\yii\web\JqueryAsset::className()]);
?>
<div class="site-index">
    <div>
        <button
            href="<?= \Yii::$app->urlManager->createUrl(['site/addsleepjob']); ?>"
            class="ajaxy-button btn btn-default"
        >
            Add SleepJob
        </button>
        <button
            href="<?= \Yii::$app->urlManager->createUrl(['site/addrandomsleepjob']); ?>"
            class="ajaxy-button btn btn-default"
        >
            Add RandomSleepJob
        </button>
    </div>
    <div
        id="table-container"
        class="site-index"
        data-url="<?= \Yii::$app->urlManager->createUrl(['site/ajaxjobs', 'page' => $page]); ?>"
    >
    <?= $this->render('_jobsTable', [
        'jobs' => $provider->getModels(),
    ]);
    ?>
    </div>
    <?= \yii\widgets\LinkPager::widget([
            'pagination' => $provider->getPagination(),
        ]);
    ?>
</div>

