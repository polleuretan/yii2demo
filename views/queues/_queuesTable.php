<?php
use yii\bootstrap\Progress;
use app\helpers\MiscHelper;

foreach ($queues as $q):
?>
  <div>
    <h3><?= $q['name']; ?></h3>
    <p>Length: <?= Progress::widget([
      'percent' => MiscHelper::limitValue($q['length'], 100),
      'label' => $q['length'],
    ]); ?></p>
    <div>
      <h6 class="pull-left">Workers: <?= count($q['pids']); ?></h6>
      <button
          href="<?= \Yii::$app->urlManager->createUrl(['queues/addworker', 'type' => $q['name']]); ?>"
          class="ajaxy-button btn btn-default pull-right"
      >
          Add worker
      </button>
    </div>

    <table class="table table-hover table-bordered">
    <?php
      if ($q['pids']):
    ?>
        <tr><th>PID</th><th>Kill worker</th></tr>
    <?php
        foreach ($q['pids'] as $pid): ?>
        <tr>
          <td><?= $pid; ?></td>
          <td style="width: 10%">
            <div class="btn-group">
              <a
                href="<?= \Yii::$app->urlManager->createUrl(['queues/killworker', 'pid' => $pid]); ?>"
                class="ajaxy-button btn btn-default btn-xs">
                <span class="glyphicon glyphicon-remove"></span>
              </a>
            </div>
          </td>
        </tr>
    <?php
        endforeach;
      else:
    ?>
        <tr><td>No workers found...</td></tr>
    <?php
      endif;
    ?>
    </table>
  </div>
<?php endforeach; ?>
