<?php
use yii\bootstrap\Progress;
use app\helpers\MiscHelper;

$this->title = 'Queues';
$host = \Yii::$app->urlManager->getHostInfo();
$this->registerJsFile($host . '/js/ajaxyContainer.js', [\yii\web\JqueryAsset::className()]);
$this->registerJsFile($host . '/js/ajaxyButton.js', [\yii\web\JqueryAsset::className()]);
// $this->registerJsFile($host . '/js/queues.js', [\yii\web\JqueryAsset::className()]);
?>
<div class="queues-index">
  <div
      id="table-container"
      class="site-index"
      data-url="<?= \Yii::$app->urlManager->createUrl(['queues/ajaxindex']); ?>"
  >
  <?= $this->render('_queuesTable', [
      'queues' => $queues,
  ]);
  ?>
  </div>
</div>
