<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SleepJob;
use app\models\RandomSleepJob;
use app\components\YiiCommand;
use app\components\BackgroundProcess;
use QueueJobs\Manager;
class QueuesController extends Controller
{
    protected $qNames = ['sleep', 'randomSleep'];
    public function behaviors()
    {
        return [];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex()
    {
        $m = new Manager();
        $queues = [];
        foreach ($this->qNames as $qName) {
            $pids = BackgroundProcess::findProcess("yii worker/spawn {$qName}");
            $q = [
                'name' => $qName,
                'length' => $m->qLen($qName),
                'pids' => $pids,
            ];
            $queues[] = $q;
        }
        return $this->render(
            'index',
            array(
                'queues' => $queues,
            )
        );
    }
    public function actionAjaxindex()
    {
        $m = new Manager();
        $queues = [];
        foreach ($this->qNames as $qName) {
            $pids = BackgroundProcess::findProcess("yii worker/spawn {$qName}");
            $q = [
                'name' => $qName,
                'length' => $m->qLen($qName),
                'pids' => $pids,
            ];
            $queues[] = $q;
        }
        return $this->renderAjax(
            '_queuesTable',
            array(
                'queues' => $queues,
            )
        );
    }
    public function actionAddworker($type){
        if (!in_array($type, $this->qNames)) {
            throw new yii\web\BadRequestHttpException('Unknown queue type', 400);
        }
        $command = new YiiCommand('worker', 'spawn');
        $command->arguments = [$type];
        $process = new BackgroundProcess($command->construct());
        $process->run();
    }
    public function actionKillworker($pid){
        $pids = BackgroundProcess::findProcess("yii worker/spawn");
        if (in_array($pid, $pids)) {
            BackgroundProcess::killProcess($pid);
        }
    }
}
