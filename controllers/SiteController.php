<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SleepJob;
use app\models\RandomSleepJob;
use app\components\YiiCommand;
use app\components\BackgroundProcess;
use QueueJobs\Manager;
class SiteController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionJobs()
    {
        $provider = $this->getJobsProvider();
        $page = \Yii::$app->request->getQueryParam('page', 1);
        return $this->render(
            'jobs',
            array(
                'provider' => $provider,
                'page' => $page,
            )
        );
    }
    public function actionAjaxjobs()
    {
        $provider = $this->getJobsProvider();
        $jobs = $provider->getModels();
        return $this->renderAjax('_jobsTable', [
            'jobs' => $jobs,
        ]);
    }
    public function actionDeletejob($id)
    {
        $m = new Manager();
        $jobs = $m->deleteJob($id);
        return $this->redirect(['site/jobs']);
    }
    public function actionAddsleepjob()
    {
        $m = new Manager();
        $j = new SleepJob('10');
        $m->addJob($j);
    }
    public function actionAddrandomsleepjob()
    {
        $m = new Manager();
        $j = new RandomSleepJob();
        $m->addJob($j);
    }
    private function getJobsProvider()
    {
        $m = new Manager();
        $jobs = $m->getJobs();
        $provider = new \yii\data\ArrayDataProvider([
            'key' => 'id',
            'allModels' => $jobs,
            'sort' => [
                'attributes' => ['queued'],
                'defaultOrder' => [
                    'queued' => SORT_DESC,
                ]
            ],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        return $provider;
    }
}
