<?php
namespace app\helpers;
class DateHelper
{
  public static function mysqlFormat($timestamp)
  {
    if ($timestamp) {
      return date('Y-m-d H:i:s', $timestamp);
    }
    return '';
  }
}