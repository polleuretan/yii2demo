<?php
namespace app\helpers;
use QueueJobs\Job;
class JobsHelper
{
  public static function textStatus($numericStatus)
  {
    if ($numericStatus) {
      return Job::$statusMap[$numericStatus];
    }
    return '';
  }
}