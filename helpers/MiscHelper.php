<?php
namespace app\helpers;
class MiscHelper
{
  public static function limitValue($value, $limit)
  {
    if ($value > $limit) {
      return $limit;
    }
    return $value;
  }
}