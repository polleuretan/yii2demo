<?php
namespace app\commands;

use yii\console\Controller;
use QueueJobs\Manager;
use QueueJobs\Job;

class WorkerController extends Controller
{
	public function actionSpawn($type)
	{
		$m = new Manager();
		$m->consumeJobs($type);
	}
}
